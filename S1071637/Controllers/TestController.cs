﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace S1071637.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["str"] = "紹宇想哭";
            ViewBag.ab = "紹宇真的在哭";
            return View();
        }
        public ActionResult Html()
        {
            return View();
        }
        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}