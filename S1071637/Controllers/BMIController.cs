﻿using S1071637.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace S1071637.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult BMI()
        {
            return View(new BMIData());
        }

        [HttpPost]

        public ActionResult BMI(BMIData data)
        {
            if (data.H < 50 || data.H > 200)
            {
                ViewBag.HError = "身高請輸入50~200的數值";
            }

            if (data.W < 30 || data.W > 150)
            {
                ViewBag.WError = "體重請輸入30~150的數值";
            }

            if (ModelState.IsValid)
            {

                var m_h = data.H / 100;
                var resule = data.W  / (m_h * m_h);
                
                var L = "";
                                            
                if(resule < 18.5)
                {
                    L = "體重過輕";
                }
                else if (18.5 <= resule && resule < 24)
                {
                    L = "正常範圍";
                }
                else if (24 <= resule && resule < 27)
                {
                    L = "過重";
                }
                else if (27 <= resule && resule < 30)
                {
                    L = "輕度肥胖";
                }
                else if (30 <= resule && resule < 35)
                {
                    L = "中度肥胖";
                }
                else if (35 <= resule)
                {
                    L = "重度肥胖";
                }
                data.L = L;
                data.R = resule;
            }
            return View(data);
        }
    }
}